package com.badepe.customchests;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by badepe on 2014.03.10..
 * Bukkit tutorial
 */
public class CustomChests extends JavaPlugin{

    public boolean turnedon = false;
    //@Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new CustomChestsListener(), this);
        getCommand("turnon").setExecutor(new CustomChestsCommands());
    }

    @Override
    public void onDisable() {
    }
}
